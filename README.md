# WORDPRESS AVEC DOCKER

Ce projet a pour but de déployer une stack Docker Wordpress avec sa base de données.
La configuration nginx-proxy

![](docs/exemple.png)

## PRE-REQUIS:
   - Linux
   - Docker
   - Docker-compose
   - Nginx
   - Certbot

### UTILISATION:

Créer en fichier .env pour y renseigner les acréditations pour que
Wordpress puisse communiquer avec sa base de données ainsi que le port de votre machine 
qui écoutera pour servir le Wordpress.
```bash
nano .env
```

Une fois toutes les variables renseignées, on les exporte pour qu'elles soient consommée par la stack:
```bash
source .env
```

Puis, on lance la stack:
```bash
docker-compose up -d
```

Visiter http://localhost:HOST_PORT/ pour finaliser l'installation de Wordpress

### HTTPS avec NGINX en proxy:

Pré-requis: Il faut un nom de domaine valide qui pointe vars votre machine,
il a dû être renseigner dans le .env

Via Certbot:

```bash
sudo apt install certbot -y
sudo certbot certonly -d ${DOMAIN_NAME}
```

Ensuite décommenter les lignes SSL comme ceci:

```bash
   ssl on;
   ssl_certificate /etc/letsencrypt/live/${DOMAIN_NAME}/fullchain.pem;
   ssl_certificate_key /etc/letsencrypt/${DOMAIN_NAME}/privkey.pem;
```
ainsi que la redirection dans le bloc HTTP comme ceci:

```bash
    # Redirection HTTPS
    return 301 https://$host$request_uri;
```
commenter le bloc location du bloc HTTP comme ceci:
```bash
   #location / {
      #proxy_redirect off;
      #proxy_set_header Host $host;
      #proxy_set_header X-Real-IP $remote_addr;
      #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      #proxy_set_header X-Forwarded-Host $server_name;
      #proxy_set_header X-Forwarded-Proto $scheme;

      #proxy_pass http://127.0.0.1:${HOST_PORT};
   #}
```
et enfin redémarrer NGINX:
```bash
sudo systemctl restart nginx
```

La dernière configuration à apporter est sur la console d'administration de Wordpress
au niveau de l'adresse URL su site (identité du site).
Y entrer l'adresse HTTPS avec votre nom de domaine dans les deux cases à renseigner:
https://nom.de.domaine.com
